#version 450 core

layout(location=0) out vec2 fs_texcoord;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
    float u = 2.0 * float(gl_VertexID & 1);
    float v = 2.0 * float(gl_VertexID >> 1);
    fs_texcoord = vec2(u, v);
    gl_Position = vec4(fs_texcoord * 2.0 - 1.0, 1.0, 1.0);
}
