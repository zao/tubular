#version 450

layout(location=0) in vec4 fs_positionM;
layout(location=1) in vec3 fs_normal;
layout(location=2) in vec2 fs_texcoord;
layout(location=3) in vec4 fs_color;

layout(location=0) out vec4 rs_color;

void main() {
  rs_color = fs_color;
  vec2 tc = fs_texcoord;
  vec3 n = normalize(fs_normal);
  float I = 0.0;
  int LightCount = 1;
  vec3 lPos[1] = vec3[](
    vec3(2.0, 2.0, -2.0)
  );
  vec3 lAtt[1] = vec3[](
    vec3(1.0, 0.0, 0.2)
  );

  for (int i = 0; i < LightCount; ++i) {
      vec3 lRel = lPos[i] - fs_positionM.xyz;
      float d = length(lRel);
      vec3 lDir = lRel / d;
      float ndotl = clamp(dot(n,lDir), 0.0, 1.0);
      vec3 atf = lAtt[i];
      float att = 1.0 / (atf.x + atf.y*d + atf.z*d*d);
      I += ndotl * att;
  }
  rs_color = vec4(fs_color.rgb * I, fs_color.a);
}
