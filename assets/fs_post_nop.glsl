#version 450 core

layout(location=0) in ivec2 fs_texcoord;

layout(location=0) out vec4 rs_color;

uniform sampler2DMS tex_color;

void main() {
    ivec2 sz = textureSize(tex_color);
    ivec2 pixcoord = ivec2(fs_texcoord * sz);
    vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
    for (int i = 0; i < 16; ++i) {
        color += textureFetch(tex_color, pixcoord, i);
    }
    rs_color = color / 16;
}
