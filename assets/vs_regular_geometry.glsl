#version 450 core

layout(location=0) in vec3 vs_position;
layout(location=1) in vec3 vs_normal;
layout(location=2) in vec2 vs_texcoord;
layout(location=3) in vec4 vs_color;

layout(location=0) out vec4 fs_positionM;
layout(location=1) out vec3 fs_normal;
layout(location=2) out vec2 fs_texcoord;
layout(location=3) out vec4 fs_color;

layout(binding=0,std140,row_major) uniform Transforms {
  mat4 P, PV, PVM;
  mat4 V, VM;
  mat4 M;
} transforms;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  vec4 positionO = vec4(vs_position, 1.0);
  gl_Position = transforms.PVM * positionO;
  fs_positionM = transforms.M * positionO;
  mat3 normalMtx = mat3(transforms.M);
  fs_normal = normalMtx * vs_normal;
  fs_texcoord = vs_texcoord;
  fs_color = vs_color;
}
