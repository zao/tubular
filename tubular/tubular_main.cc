#include <stb_image.h>
#include <par_shapes.h>
#include <MathGeoLib/MathGeoLib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include <GL/gl3w.h>

#include <algorithm>
#include <array>
#include <fstream>
#include <vector>

#include <stdint.h>
#include <stdio.h>

#include "files.h"
#include "program_loader.h"
#include "zone_map.h"

#if !_WIN32
#define DebugBreak() __builtin_trap()
#endif

namespace sdl {
    struct InitQuit {
        explicit InitQuit(uint32_t flags) {
            SDL_Init(flags);
        }

        ~InitQuit() {
            SDL_Quit();
        }

    private:
        InitQuit(InitQuit const&) = delete;
        InitQuit& operator = (InitQuit const&) = delete;
    };

    struct GLAttributes {
        GLAttributes& Set(SDL_GLattr attribute, int value) {
            SDL_GL_SetAttribute(attribute, value);
            return *this;
        }
    };
}

namespace gl {
    std::vector<char> GetProgramInfoLog(GLuint prog) {
        GLint cb{};
        glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &cb);
        std::vector<char> ret(cb+1);
        glGetProgramInfoLog(prog, cb, nullptr, ret.data());
        return ret;
    }
}

void Mention(std::string message) {
#if _WIN32
    OutputDebugStringA(message.c_str());
    OutputDebugStringA("\n");
#else
    fprintf(stderr, "%s\n", message.c_str());
    fflush(stderr);
#endif
}

void Mention(std::ostringstream& os) {
    Mention(os.str());
}

float TranslateFullAxis(int16_t value) {
    if (value == INT16_MIN) ++value;
    int sign = value<0 ? -1 : +1;
    if (abs(value) < 0x1000) value = 0;
    else value = sign * (abs(value) - 0x1000);
    return value / static_cast<float>(INT16_MAX/2 - 0x1000);
}

float TranslateHalfAxis(int16_t value) {
    uint16_t v = value + INT16_MIN;
    return v / static_cast<float>(INT16_MAX);
}

struct Vertex {
    float3 position; float3 normal; float2 texcoord; float4 tint;
};

auto MakePlane = [](float size, float4 tint) -> std::array<Vertex, 6> {
    auto* plane = par_shapes_create_plane(1, 1);
    par_shapes_translate(plane, -0.5f, -0.5f, 0.0f);
    par_shapes_scale(plane, size, size, size);
    par_shapes_unweld(plane, true);
    par_shapes_compute_normals(plane);
    if (plane->npoints != 6) std::terminate();
    std::array<Vertex, 6> vertices;
    for (size_t i = 0; i < 6; ++i) {
        auto& vtx = vertices[i];
        float* pos = plane->points + i*3;
        float* n = plane->normals + i*3;
        vtx.position = float3(pos[0], pos[2], -pos[1]);
        vtx.normal = float3(n[0], n[2], -n[1]);
        memcpy(vtx.texcoord.ptr(), plane->tcoords + i*2, sizeof(float2));
        vtx.tint = tint;
    }
    par_shapes_free_mesh(plane);
    return vertices;
};

auto MakeCube = [](float size, float4 tint) -> std::array<Vertex, 36> {
    auto* cube = par_shapes_create_cube();
    par_shapes_translate(cube, -0.5f, -0.5f, -0.5f);
    par_shapes_scale(cube, size, size, size);
    par_shapes_unweld(cube, true);
    par_shapes_compute_normals(cube);
    std::array<Vertex, 36> vertices;
    if (cube->npoints != 36) std::terminate();
    for (size_t i = 0; i < 36; ++i) {
        auto& vtx = vertices[i];
        memcpy(vtx.position.ptr(), cube->points + i*3, sizeof(float3));
        memcpy(vtx.normal.ptr(), cube->normals + i*3, sizeof(float3));
        vtx.texcoord = float2::zero;
        vtx.tint = tint;
    }
    par_shapes_free_mesh(cube);
    return vertices;
};

struct Mesh {
    Mesh(Vertex const* vertices, size_t vertexCount)
            : vertexCount(vertexCount)
    {
        GLint lastVB;
        glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &lastVB);
        glCreateBuffers(1, &vbo);
        glNamedBufferStorage(vbo, sizeof(Vertex) * vertexCount, vertices, 0);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBindBuffer(GL_ARRAY_BUFFER, lastVB);
    }

    ~Mesh() {
        glDeleteBuffers(1, &vbo);
    }

    GLuint vbo;
    size_t vertexCount;

private:
    Mesh(Mesh const&) = delete;
    Mesh& operator = (Mesh const&) = delete;
};

std::unique_ptr<Mesh> MakeMesh(Vertex const* data, size_t size) {
    auto ret = std::make_unique<Mesh>(data, size);
	return ret;
}

template <typename Vertices>
std::unique_ptr<Mesh> MakeMesh(Vertices const& vertices) {
    auto ret = std::make_unique<Mesh>(vertices.data(), vertices.size());
    return ret;
}

struct GrassMeshFactory : zone::MeshFactory {
    Mesh* MakeLevelPlate() override {
        return levelPlate;
    }

    Mesh* MakePathBlip() override {
        return pathBlip;
    }

    Mesh* levelPlate;
    Mesh* pathBlip;
};

struct Input {
    float2 leftStick = float2::zero, rightStick = float2::zero;
    float leftTrigger = 0.0f, rightTrigger = 0.0f;
    float lastDPadPress[4] = { -FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX };
};

struct GameScreen;
using GameScreenPtr = std::unique_ptr<GameScreen>;
struct GameScreen {
    virtual ~GameScreen() {}

    virtual void InputStage(float now, Input input) = 0;
    virtual void UpdateStage(float2 windowSize) = 0;
    virtual void DrawStage() = 0;
};

struct GlobalGameState {
};

struct Transforms {
    float4x4 P, PV, PVM;
    float4x4 V, VM;
    float4x4 M;
};

struct ZoneMapScreen : GameScreen {
    std::unique_ptr<zone::Map> map;
    Transforms transforms;
    Frustum cameraFrustum;
    float windowWidth{}, windowHeight{};
    bool sizeChanged{};

    GLuint prepassFBO{};
    GLuint prepassColorTarget{}, prepassDepthTarget{};

    GLuint vao{};
    util::ObjectHandle pipe;
    GLuint transformUB{};

    float2 camRot = float2::zero;
    float2 camXZ = float2::zero;

    struct PlayerMovement {
        zone::Id from{};
        zone::Id to{};
        float perc{};
    };
    PlayerMovement playerMovement;
    zone::MapVisual::Static playerVisual;
    Input input{};

    float const dt = 1.0f/60.0f;
    float now{};

    ZoneMapScreen() {
        playerMovement.from = 103;
        playerMovement.to = 103;
        playerMovement.perc = 1.0f;

        glCreateFramebuffers(1, &prepassFBO);
        cameraFrustum.SetKind(FrustumSpaceGL, FrustumLeftHanded);
        cameraFrustum.SetViewPlaneDistances(0.1f, 1000.0f);

        glCreateVertexArrays(1, &vao);
        glBindVertexArray(vao);

        pipe = util::MakeProgramPipeline();
        {
            util::ObjectHandle vsProgram, fsProgram;
            vsProgram = util::SeparableProgramLoader("vs_regular_geometry.glsl").Build(GL_VERTEX_SHADER);
            auto vsLog = gl::GetProgramInfoLog(*vsProgram);
            Mention(vsLog.data());

            fsProgram = util::SeparableProgramLoader("fs_boringly_lit.glsl").Build(GL_FRAGMENT_SHADER);
            auto fsLog = gl::GetProgramInfoLog(*fsProgram);
            Mention(fsLog.data());

            glUseProgramStages(*pipe, GL_VERTEX_SHADER_BIT, *vsProgram);
            glUseProgramStages(*pipe, GL_FRAGMENT_SHADER_BIT, *fsProgram);
        }

        glCreateBuffers(1, &transformUB);
        glNamedBufferStorage(transformUB, sizeof(Transforms), &transforms, GL_DYNAMIC_STORAGE_BIT);

        glUniformBlockBinding(*pipe, glGetUniformBlockIndex(*pipe, "Transforms"), 0);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, transformUB);
    }

    void InputStage(float now, Input input) override {
        this->now = now;
        this->input = input;
    }

    void UpdateStage(float2 windowSize) override {
        if (windowWidth != windowSize[0] || windowHeight != windowSize[1]) {
            windowWidth = windowSize[0];
            windowHeight = windowSize[1];
            sizeChanged = true;
        }
        float3 const PlayerOffset = float3(0.0f, 0.6f, 0.0f);
        float const PlayerSpeed = 2.0f;
        playerMovement.perc = (std::min)(1.0f, playerMovement.perc + dt * PlayerSpeed);

        float const MoveSlopAllowed = 0.1f;
        if (playerMovement.perc == 1.0f) {
            float moveCutoff = now - MoveSlopAllowed;
            bool anyDirSet{};
            zone::Direction dir{};
            for (size_t i = 0; i < 4; ++i) {
                if (moveCutoff <= input.lastDPadPress[i]) {
                    moveCutoff = input.lastDPadPress[i];
                    anyDirSet = true;
                    dir = (zone::Direction)i;
                }
            }
            if (anyDirSet) {
                auto* currentNode = map->graph.FindNode(playerMovement.to);
                auto* target = currentNode->exits[(int)dir];
                if (target) {
                    input.lastDPadPress[(int)dir] = 0.0f;
                    playerMovement.from = currentNode->id;
                    playerMovement.to = target->id;
                    playerMovement.perc = 0.0f;
                }
            }
        }

        {
            auto mov = playerMovement;
            playerVisual.pos = Lerp(
                    map->visual.FindLevel(mov.from)->pos,
                    map->visual.FindLevel(mov.to)->pos,
                    mov.perc) + PlayerOffset;
        }

        float2 camRot = float2(-1.0f, -1.0f).Mul(input.rightStick);
        camXZ += float2(1.0f, -1.0f).Mul(input.leftStick) * 10.0f * dt;

        {
            float3x4 camM =
                    float3x4::Translate(camXZ[0], 0.3f, camXZ[1]) *
                    float3x4::RotateY(camRot.x) *
                    float3x4::RotateX(Clamp(camRot.y, -0.6f, 0.6f)) *
                    float3x4::RotateX(1.0f) *
                    float3x4::Translate(0.0f, 0.0f, -6.0f);
            cameraFrustum.SetWorldMatrix(camM);
        }
    }

    void DrawStage() override {
        if (sizeChanged) {
            sizeChanged = false;
            float const windowAspect = windowWidth / windowHeight;
            cameraFrustum.SetHorizontalFovAndAspectRatio(pi/2.0f, windowAspect);
            {
                if (prepassColorTarget) glDeleteTextures(1, &prepassColorTarget);
                if (prepassDepthTarget) glDeleteTextures(1, &prepassDepthTarget);
                glCreateTextures(GL_TEXTURE_2D_MULTISAMPLE, 1, &prepassColorTarget);
                glCreateTextures(GL_TEXTURE_2D_MULTISAMPLE, 1, &prepassDepthTarget);
                glTextureStorage2DMultisample(prepassColorTarget, 2, GL_RGBA8, windowWidth, windowHeight, GL_FALSE);
                glTextureStorage2DMultisample(prepassDepthTarget, 2, GL_DEPTH_COMPONENT32, windowWidth, windowHeight, GL_FALSE);
                glNamedFramebufferTexture(prepassFBO, GL_COLOR_ATTACHMENT0, prepassColorTarget, 0);
                glNamedFramebufferTexture(prepassFBO, GL_DEPTH_ATTACHMENT, prepassDepthTarget, 0);
                auto status = glCheckNamedFramebufferStatus(prepassFBO, GL_DRAW_FRAMEBUFFER);
                if (status != GL_FRAMEBUFFER_COMPLETE) std::terminate();
            }
        }

        transforms.P = cameraFrustum.ComputeProjectionMatrix();
        transforms.V = cameraFrustum.ComputeViewMatrix();
        transforms.PV = transforms.P * transforms.V;
        transforms.M = float4x4::identity;
        transforms.PVM = transforms.PV * transforms.M;
        transforms.VM = transforms.V * transforms.M;

        glBindVertexArray(vao);

        // Draw to pre pass
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, prepassFBO);
        {
            glViewport(0, 0, windowWidth, windowHeight);
            float4 clearColor(0.0f, 0.0f, 0.0f, 1.0f);
            glClearBufferfv(GL_COLOR, 0, clearColor.ptr());
            float clearDepth = 1.0f;
            glClearBufferfv(GL_DEPTH, 0, &clearDepth);
        }
        {
            glEnable(GL_DEPTH_TEST);
            glBindProgramPipeline(*pipe);
            for (size_t i = 0; i < 4; ++i) {
                glEnableVertexArrayAttrib(vao, i);
                glVertexArrayAttribBinding(vao, i, 0);
                glVertexArrayBindingDivisor(vao, i, 0);
            }
            glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, position));
            glVertexArrayAttribFormat(vao, 1, 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, normal));
            glVertexArrayAttribFormat(vao, 2, 2, GL_FLOAT, GL_FALSE, offsetof(Vertex, texcoord));
            glVertexArrayAttribFormat(vao, 3, 4, GL_FLOAT, GL_FALSE, offsetof(Vertex, tint));

            GLuint lastVBO = 0;
            auto const DrawThing = [&](auto& s) {
                auto vbo = s.mesh->vbo;
                if (lastVBO != vbo) {
                    glVertexArrayVertexBuffer(vao, 0, vbo, 0, sizeof(Vertex));
                    lastVBO = vbo;
                }
                transforms.M =
                        float4x4::Translate(s.pos) *
                        s.orientation.ToFloat3x4();
                transforms.PVM = transforms.PV * transforms.M;
                transforms.VM = transforms.V * transforms.M;
                glNamedBufferSubData(transformUB, 0, sizeof(Transforms), &transforms);
                glDrawArrays(GL_TRIANGLES, 0, s.mesh->vertexCount);
            };

            for (auto& s : map->visual.levels) { DrawThing(s); }
            for (auto& s : map->visual.statics) { DrawThing(s); }
            DrawThing(playerVisual);
        }

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        {
            glViewport(0, 0, windowWidth, windowHeight);
            float4 clearColor(0.0f, 0.0f, 0.0f, 1.0f);
            glClearBufferfv(GL_COLOR, 0, clearColor.ptr());
            float clearDepth = 1.0f;
            glClearBufferfv(GL_DEPTH, 0, &clearDepth);
        }
        glBlitNamedFramebuffer(prepassFBO, 0,
                               0, 0, windowWidth, windowHeight,
                               0, 0, windowWidth, windowHeight,
                               GL_COLOR_BUFFER_BIT,
                               GL_NEAREST);
    }
};

#if _WIN32
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
#else
int main() {
#endif
    sdl::InitQuit iq(SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER | SDL_INIT_JOYSTICK | SDL_INIT_TIMER | SDL_INIT_VIDEO);
    sdl::GLAttributes()
            .Set(SDL_GL_CONTEXT_MAJOR_VERSION, 4)
            .Set(SDL_GL_CONTEXT_MINOR_VERSION, 5)
            .Set(SDL_GL_RED_SIZE, 8)
            .Set(SDL_GL_GREEN_SIZE, 8)
            .Set(SDL_GL_BLUE_SIZE, 8)
            .Set(SDL_GL_DEPTH_SIZE, 24)
            .Set(SDL_GL_DOUBLEBUFFER, 1)
            .Set(SDL_GL_MULTISAMPLEBUFFERS, 0)
            .Set(SDL_GL_MULTISAMPLESAMPLES, 0)
            .Set(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE)
            .Set(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
    int windowWidth{1280}, windowHeight{720};
    float windowAspect{static_cast<float>(windowWidth) / static_cast<float>(windowHeight)};
    SDL_Window* window = SDL_CreateWindow("tubular", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                          windowWidth, windowHeight,
                                          SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);
    if (!window) DebugBreak();

    SDL_GLContext ctx = SDL_GL_CreateContext(window);
    gl3wInit();

    LCG lcg{9002u};

    auto groundPlane = MakeMesh(MakePlane(200.0f, float4(0.0f, 0.48f, 0.047f, 1.0f)));
    auto levelPlate = MakeMesh(MakeCube(0.5f, float4(1.0f, 0.84f, 0.0f, 1.0f)));
    auto pathBlip = MakeMesh(MakeCube(0.2f, float4(1.0f, 0.84f, 0.0f, 1.0f)));
    auto playerMesh = MakeMesh(MakeCube(0.4f, float4(1.0f, 0.84f, 0.6f, 1.0f)));

    std::array<std::unique_ptr<Mesh>, 7> rockMeshes;
    for (auto& m : rockMeshes) {
        auto rock = par_shapes_create_rock(lcg.IntFast(), 2);
        par_shapes_scale(rock, 0.2f, 0.2f, 0.2f);
        par_shapes_unweld(rock, true);
        par_shapes_compute_normals(rock);
        std::vector<Vertex> vertices(rock->npoints);
        for (size_t i = 0; i < rock->npoints; ++i) {
            auto& vtx = vertices[i];
            memcpy(vtx.position.ptr(), rock->points + i*3, sizeof(float3));
            memcpy(vtx.normal.ptr(), rock->normals + i*3, sizeof(float3));
            vtx.texcoord = float2(0.0f, 0.0f);
            vtx.tint = float4(0.59f, 0.29f, 0.0f, 1.0f);
        }
        m = MakeMesh(vertices);
        par_shapes_free_mesh(rock);
    }

    std::shared_ptr<ZoneMapScreen> zms = std::make_shared<ZoneMapScreen>();
    zms->playerVisual.mesh = playerMesh.get();

    GrassMeshFactory zoneMeshes;
    zoneMeshes.levelPlate = levelPlate.get();
    zoneMeshes.pathBlip = pathBlip.get();

    {
        {
            while (true) {
                auto vec = SlurpTextAsset("zone_1.json");
                if (!vec.success) { DebugBreak(); continue; }
                Json::Reader r;
                Json::Value root;
                if (!r.parse(vec.data.data(), vec.data.data() + vec.data.size(), root)) { DebugBreak(); continue; }
                auto map = zone::LoadMap(root, &zoneMeshes);
                if (!map) { DebugBreak(); continue; }
                zms->map = std::move(map);
                break;
            }
        }

        for (size_t i = 0; i < 100; ++i) {
            auto pos = float3::RandomBox(lcg, float3(-10.0f, -0.1f, -10.0f), float3(+10.0f, +0.0f, +10.0f));
            auto orientation = Quat::RotateY(lcg.Float(0.0f, 2.0f*pi));
            auto mesh = rockMeshes[lcg.Int(0, rockMeshes.size()-1)].get();
            zms->map->visual.AddStatic(pos, orientation, mesh);
        }
        {
            auto pos = float3::zero;
            auto orientation = Quat::identity;
            auto mesh = groundPlane.get();
            zms->map->visual.AddStatic(pos, orientation, mesh);
        }
    }

    auto stick = SDL_JoystickOpen(0);

    float dt = 1.0f / 60.0f;

    Input input{};

    bool running = true;
    while (1) {
        SDL_PumpEvents();
        SDL_Event ev{};
        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_KEYUP) {
                if (ev.key.keysym.sym == SDLK_ESCAPE) {
                    running = false;
                }
            }
            if (ev.type == SDL_QUIT) {
                running = false;
                break;
            }
        }
        if (!running) break;

        float now = SDL_GetTicks() / 1000.0f;

        input.leftStick.x = TranslateFullAxis(SDL_JoystickGetAxis(stick, 0));
        input.leftStick.y = TranslateFullAxis(SDL_JoystickGetAxis(stick, 1));
        input.leftTrigger = TranslateHalfAxis(SDL_JoystickGetAxis(stick, 2));
        input.rightStick.x = TranslateFullAxis(SDL_JoystickGetAxis(stick, 3));
        input.rightStick.y = TranslateFullAxis(SDL_JoystickGetAxis(stick, 4));
        input.rightTrigger = TranslateHalfAxis(SDL_JoystickGetAxis(stick, 5));

        {
            auto hat = SDL_JoystickGetHat(stick, 0);
            if (hat&SDL_HAT_LEFT) { input.lastDPadPress[(size_t)zone::Direction::Left] = now; }
            if (hat&SDL_HAT_UP) { input.lastDPadPress[(size_t)zone::Direction::Up] = now; }
            if (hat&SDL_HAT_RIGHT) { input.lastDPadPress[(size_t)zone::Direction::Right] = now; }
            if (hat&SDL_HAT_DOWN) { input.lastDPadPress[(size_t)zone::Direction::Down] = now; }
        }

        zms->InputStage(now, input);

        {
            int w{}, h{};
            SDL_GetWindowSize(window, &w, &h);
            float2 ws{static_cast<float>(w), static_cast<float>(h)};
            zms->UpdateStage(ws);
        }

        zms->DrawStage();
        SDL_GL_SwapWindow(window);
    }
    SDL_JoystickClose(stick);
    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(window);
    return 0;
}