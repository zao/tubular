#pragma once

#include <memory>
#include <string>
#include <GL/gl3w.h>

namespace util {
    using ObjectHandle = std::shared_ptr<GLuint>;

    ObjectHandle MakeSeparableProgram(GLenum type);
    ObjectHandle MakeProgramPipeline();

    struct SeparableProgramLoader {
        explicit SeparableProgramLoader(std::string filename);

        ObjectHandle Build(GLenum type);

    private:
        std::string filename;
    };
};