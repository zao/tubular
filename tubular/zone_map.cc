#include "zone_map.h"

#include <algorithm>

namespace zone {
    struct MatchId {
        MatchId(Id id) : id(id) {}
        Id id;

        bool operator () (MapGraph::NodePtr const& node) const {
            return node->id == this->id;
        }

        bool operator () (MapVisual::Level const& level) const {
            return level.id == this->id;
        }
    };

    void MapGraph::AddNode(Id id) {
        auto n = std::make_unique<zone::MapGraph::Node>();
        n->id = id;
        nodes.push_back(std::move(n));
    }

    void MapGraph::Connect(MapGraph::Connection conn) {
        auto from = FindNode(conn.nodeFrom);
        auto to = FindNode(conn.nodeTo);
        if (from && to) {
            from->exits[(size_t)conn.exitFrom] = to;
            to->exits[(size_t)conn.exitTo] = from;
        }
    }

    void MapVisual::AddLevel(Id id, float3 pos, Quat orientation, Mesh* mesh) {
        zone::MapVisual::Level s{};
        s.id = id;
        s.pos = pos;
        s.orientation = orientation;
        s.mesh = mesh;
        levels.push_back(std::move(s));
    }

    void MapVisual::AddStatic(float3 pos, Quat orientation, Mesh* mesh) {
        zone::MapVisual::Static s{};
        s.pos = pos;
        s.orientation = orientation;
        s.mesh = mesh;
        statics.push_back(std::move(s));
    }

    MapVisual::Level const* MapVisual::FindLevel(Id id) const {
        auto I = std::find_if(begin(levels), end(levels), MatchId(id));
        if (I != end(levels)) return &*I;
        return nullptr;
    }

    MapGraph::Node* MapGraph::FindNode(Id id) const {
        auto I = std::find_if(begin(nodes), end(nodes), MatchId(id));
        if (I != end(nodes)) return I->get();
        return nullptr;
    }

    std::unique_ptr<Map> LoadMap(Json::Value const& root, MeshFactory* factory) {
        auto const AsDirection = [](Json::Value const& value) -> Direction {
            if (value.isString()) {
                auto txt = value.asString();
                if (txt == "left" || txt == "west") return Direction::Left;
                if (txt == "up" || txt == "north") return Direction::Up;
                if (txt == "right" || txt == "east") return Direction::Right;
                if (txt == "down" || txt == "south") return Direction::Down;
                std::terminate();
            }
            uint64_t v = value.asUInt64();
            if (v <= 3) return static_cast<Direction>(v);
            std::terminate();
        };
        auto const AsFloat3 = [](Json::Value const& value) -> float3 {
            if (value.size() == 3) {
                if (value[0].isNumeric() && value[1].isNumeric() && value[2].isNumeric()) {
                    float3 ret = {
                            value[0].asFloat(), value[1].asFloat(), value[2].asFloat()
                    };
                    return ret;
                }
            }
            std::terminate();
        };
        std::unique_ptr<Map> ret = std::make_unique<Map>();
        for (auto& L : root["levels"]) {
            auto id = L["id"].asInt64();
            auto pos = AsFloat3(L["pos"]);
            ret->graph.AddNode(id);
            ret->visual.AddLevel(id, pos, Quat::identity, factory->MakeLevelPlate());
        }
        for (auto& C : root["connections"]) {
            MapGraph::Connection conn{};
            conn.nodeFrom = C["src_id"].asInt64();
            conn.nodeTo = C["dst_id"].asInt64();
            conn.exitFrom = AsDirection(C["src_side"]);
            conn.exitTo = AsDirection(C["dst_side"]);
            ret->graph.Connect(conn);

            auto visFrom = ret->visual.FindLevel(conn.nodeFrom);
            auto visTo = ret->visual.FindLevel(conn.nodeTo);
            if (visFrom && visTo) {
                float plateDist = visFrom->pos.Distance(visTo->pos);
                size_t const PathSteps = 1 + static_cast<size_t>(plateDist * 2.0f);
                for (size_t i = 0; i < PathSteps; ++i) {
                    float perc = (i+1) / static_cast<float>(PathSteps+1);
                    float3 pos = Lerp(visFrom->pos, visTo->pos, perc);
                    pos.y = -0.05f;
                    zone::MapVisual::Static s{};
                    s.pos = pos;
                    s.orientation = Quat::identity;
                    s.mesh = factory->MakePathBlip();
                    ret->visual.statics.push_back(std::move(s));
                };
            }
        }

        return ret;
    }
}