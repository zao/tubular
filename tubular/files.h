#pragma once

#include <stddef.h>
#include <fstream>
#include <string>
#include <vector>

struct SlurpResult {
    bool success;
    std::vector<char> data;
};

inline SlurpResult SlurpTextAsset(std::string name) {
    std::ifstream is("assets/" + name, std::ios::binary);
    if (!is) return {};
    is.seekg(0, std::ios::end);
    size_t cb = static_cast<size_t>(is.tellg());
    is.seekg(0, std::ios::beg);
    SlurpResult ret{true};
    ret.data.resize(cb + 1);
    is.read(ret.data.data(), cb);
    return ret;
}