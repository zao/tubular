#include "program_loader.h"
#include "files.h"

namespace util {
    ObjectHandle MakeProgram() {
        GLuint id{};
        id = glCreateProgram();
        ObjectHandle ret(new GLuint(id), [](GLuint* p) { glDeleteProgram(*p); delete p; });
        return ret;
    }

    ObjectHandle MakeShaderProgram(GLenum type, char const* data) {
        GLuint id;
        id = glCreateShaderProgramv(type, 1, &data);
        ObjectHandle ret(new GLuint(id), [](GLuint* p) { glDeleteProgram(*p), delete p; });
        return ret;
    }

    ObjectHandle MakeProgramPipeline() {
        GLuint id{};
        glCreateProgramPipelines(1, &id);
        ObjectHandle ret(new GLuint(id), [](GLuint* p) { glDeleteProgramPipelines(1, p); delete p; });
        return ret;
    };

    SeparableProgramLoader::SeparableProgramLoader(std::string filename)
    : filename(filename)
    {}

    ObjectHandle SeparableProgramLoader::Build(GLenum type) {
        while (true) {
            std::vector<char> data;
            while (true) {
                auto result = SlurpTextAsset(filename);
                if (result.success) {
                    data.swap(result.data);
                    break;
                }
            }
            ObjectHandle ret = MakeShaderProgram(type, data.data());
            GLint linked{};
            glGetProgramiv(*ret, GL_LINK_STATUS, &linked);
            if (linked == GL_TRUE) {
                return ret;
            }
        }
    }
}