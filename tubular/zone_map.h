#pragma once

#include <stdint.h>
#include <MathGeoLib/MathGeoLib.h>
#include <json/json.h>

#include <vector>

struct Mesh;

namespace zone {
    using Id = uint64_t;

    struct MapVisual {
        struct Static {
            float3 pos;
            Quat orientation;
            Mesh* mesh;
        };

        struct Level : Static {
            Id id;
        };

        void AddLevel(Id id, float3 pos, Quat orientation, Mesh* mesh);
        void AddStatic(float3 pos, Quat orientation, Mesh* mesh);

        Level const* FindLevel(Id id) const;

        std::vector<Level> levels;
        std::vector<Static> statics;
    };

    enum class Direction { Left, Up, Right, Down };

    struct MapGraph {
        struct Node {
            Id id;
            Node* exits[4]{};
        };

        struct Connection {
            Id nodeFrom, nodeTo;
            Direction exitFrom, exitTo;
        };

        void AddNode(Id id);
        void Connect(Connection conn);

        Node* FindNode(Id id) const;

        using NodePtr = std::unique_ptr<Node>;
        std::vector<NodePtr> nodes;
    };

    struct Map {
        MapGraph graph;
        MapVisual visual;
    };

    struct MeshFactory {
        virtual ~MeshFactory() {}

        virtual Mesh* MakeLevelPlate() = 0;
        virtual Mesh* MakePathBlip() = 0;
    };

    std::unique_ptr<Map> LoadMap(Json::Value const& root, MeshFactory* factory);
};